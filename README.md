# README #

This is a simple test to use on webtask. I'm calling to the public API from yahoo with a simple query and getting stock options information of some companies.

### Want to try? ###

The next URL will return stock data of: Yahoo (YHOO), Apple (AAPL), Google (GOOG), and Microsoft (MSFT).
https://webtask.it.auth0.com/api/run/wt-maxzelarayan-gmail_com-0/stocks?webtask_no_cache=1

If yo want to filter by one of them just add the 'company' parameter to the url, see example:
https://webtask.it.auth0.com/api/run/wt-maxzelarayan-gmail_com-0/stocks?webtask_no_cache=1&company=AAPL

Feel free to try with a company that is not on the list.

### Who do I talk to? ###

* Max - [maxzelarayan.com](http://maxzelarayan.com/)
