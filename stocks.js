const request = require('request');

module.exports = function(context, callback) {
  var url = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22YHOO%22%2C%22AAPL%22%2C%22GOOG%22%2C%22MSFT%22)%0A%09%09&format=json&diagnostics=true&env=http%3A%2F%2Fdatatables.org%2Falltables.env&callback=';
  var aux = {};

  request(url, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var info = JSON.parse(body);

        if(context.data.company) {
            aux = info.query.results.quote.filter(function(item) {
                return item.symbol === context.data.company;
            });

            aux = aux.length ? aux : 'No results found using: ' + context.data.company;
        } else {
            aux = info.query.results.quote;
        }

        callback(null, aux);

      }
  });
};
